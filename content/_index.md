## Página principal

Este blog está alojado en [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) y se construye en menos de 1 minuto.

En este momento esta usando el tema «beautifulhugo».

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
