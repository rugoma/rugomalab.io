---
title: Sobre mi
subtitle: Ingeniero técnico y profe de robótica
comments: false
---

Me llamo Rubén Gómez Antolí.

Este blog es una prueba que estamos haciendo en las clases de robótica. 
Aprendemos sobre:

- Aprendemos sobre robótica
- Aprendemos sobre git
- Aprendemos sobre RenPy
- Aprendemos sobre Hugo, ¡para hacer un blog!

### Mi biografía

No hay mucho que contar, estudié mucho, trabajé mucho, aprendí mucho: 
electrónica, informática y miles de cosas más.